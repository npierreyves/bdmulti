create schema if not exists SCHEMA1_DB2;
create schema if not exists SCHEMA2_DB2;

create table if not exists schema1_db2.reference
(
    id    serial
        constraint reference_pk
            primary key,
    label varchar(256)
);
create unique index if not exists reference_id_uindex on schema1_db2.reference (id);

create table if not exists schema2_db2.project
(
    id    serial
        constraint project_pk
        primary key,
    title varchar(50)
);
create unique index if not exists project_id_uindex on schema2_db2.project (id);

create sequence if not exists HIBERNATE_SEQUENCE;