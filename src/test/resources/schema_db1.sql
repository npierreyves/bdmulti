create schema if not exists SCHEMA1_DB1;
create schema if not exists SCHEMA2_DB1;

create table if not exists schema1_db1.user
(
    id   serial
    constraint user_pk
    primary key,
    name varchar(128) not null
);
create unique index if not exists user_id_uindex on schema1_db1.user (id);

create table if not exists schema2_db1.product
(
    id    serial
        constraint product_pk
            primary key,
    label varchar(512) not null
);
create unique index if not exists product_id_uindex on schema2_db1.product (id);

drop sequence if exists HIBERNATE_SEQ;
create sequence if not exists HIBERNATE_SEQUENCE;