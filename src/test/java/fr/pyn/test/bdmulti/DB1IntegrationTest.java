package fr.pyn.test.bdmulti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import fr.pyn.test.bdmulti.domain.db1.Product;
import fr.pyn.test.bdmulti.domain.db1.User;
import fr.pyn.test.bdmulti.repository.db1.ProductRepository;
import fr.pyn.test.bdmulti.repository.db1.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes=BdmultiApplication.class)
@EnableTransactionManagement
@ActiveProfiles("test")
public class DB1IntegrationTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional("db1TransactionManager")
    public void whenCreatingUser_thenCreated() {
        User user = new User("user_test");
        user = userRepository.save(user);

        final Optional<User> result = userRepository.findById(user.getId());
        assertTrue(result.isPresent());
        assertEquals(user.getId(), result.get().getId());
        assertEquals("user_test", result.get().getName());
        assertEquals(user.toString(), result.get().toString());
    }

    @Test
    @Transactional("db1TransactionManager")
    public void whenCreatingProduct_thenCreated() {
        Product product = new Product("product 1");
        product = productRepository.save(product);

        final Optional<Product> result = productRepository.findById(product.getId());
        assertTrue(result.isPresent());
        assertEquals(product.getId(), result.get().getId());
        assertEquals("product 1", result.get().getLabel());
        assertEquals(product.toString(), result.get().toString());
    }
}