package fr.pyn.test.bdmulti;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class BdmultiApplicationTests {

	@Test
	void contextLoads() {
		BdmultiApplication.main(new String[] {});
	}

}