package fr.pyn.test.bdmulti;

import fr.pyn.test.bdmulti.domain.db1.Product;
import fr.pyn.test.bdmulti.domain.db1.User;
import fr.pyn.test.bdmulti.domain.db2.Project;
import fr.pyn.test.bdmulti.domain.db2.Reference;
import fr.pyn.test.bdmulti.repository.db1.ProductRepository;
import fr.pyn.test.bdmulti.repository.db1.UserRepository;
import fr.pyn.test.bdmulti.repository.db2.ProjectRepository;
import fr.pyn.test.bdmulti.repository.db2.ReferenceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes=BdmultiApplication.class)
@EnableTransactionManagement
@ActiveProfiles("test")
public class DB2IntegrationTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ReferenceRepository referenceRepository;

    @Test
    @Transactional("db2TransactionManager")
    public void whenCreatingUser_thenCreated() {
        Project project = new Project("project 1");
        project = projectRepository.save(project);

        final Optional<Project> result = projectRepository.findById(project.getId());
        assertTrue(result.isPresent());
        assertEquals(project.getId(), result.get().getId());
        assertEquals("project 1", result.get().getTitle());
        assertEquals(project.toString(), result.get().toString());
    }

    @Test
    @Transactional("db2TransactionManager")
    public void whenCreatingProduct_thenCreated() {
        String ref = UUID.randomUUID().toString();
        Reference reference = new Reference(ref);
        reference = referenceRepository.save(reference);

        final Optional<Reference> result = referenceRepository.findById(reference.getId());
        assertTrue(result.isPresent());
        assertEquals(reference.getId(), result.get().getId());
        assertEquals(ref, result.get().getLabel());
        assertEquals(reference.toString(), result.get().toString());
    }
}