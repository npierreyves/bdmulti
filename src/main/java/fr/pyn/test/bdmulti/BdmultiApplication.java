package fr.pyn.test.bdmulti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdmultiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdmultiApplication.class, args);
	}
}
