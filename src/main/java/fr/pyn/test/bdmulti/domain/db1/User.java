package fr.pyn.test.bdmulti.domain.db1;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "schema1_db1")
public class User {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  private String name;

  protected User() {}

  public User(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format(
        "User[id=%d, name='%s']",
        id, name);
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }
}