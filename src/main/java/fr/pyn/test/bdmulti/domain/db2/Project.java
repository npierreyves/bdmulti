package fr.pyn.test.bdmulti.domain.db2;

import javax.persistence.*;

@Entity
@Table(name = "project", schema = "schema2_db2")
public class Project {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  private String title;

  protected Project() {}

  public Project(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return String.format(
        "Project[id=%d, title='%s']",
        id, title);
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }
}