package fr.pyn.test.bdmulti.domain.db2;

import javax.persistence.*;

@Entity
@Table(name = "reference", schema = "schema1_db2")
public class Reference {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  private String label;

  protected Reference() {}

  public Reference(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return String.format(
        "Reference[id=%d, label='%s']",
        id, label);
  }

  public Long getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}