package fr.pyn.test.bdmulti.domain.db1;

import javax.persistence.*;

@Entity
@Table(name = "product", schema = "schema2_db1")
public class Product {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  private String label;

  protected Product() {}

  public Product(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return String.format(
        "Product[id=%d, label='%s']",
        id, label);
  }

  public Long getId() {
    return id;
  }

  public String getLabel() {
    return label;
  }
}