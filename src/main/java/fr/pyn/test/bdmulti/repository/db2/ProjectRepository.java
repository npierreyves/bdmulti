package fr.pyn.test.bdmulti.repository.db2;

import fr.pyn.test.bdmulti.domain.db2.Project;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    List<Project> findByTitle(String title);

    Project findById(long id);
}