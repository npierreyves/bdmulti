package fr.pyn.test.bdmulti.repository.db1;

import java.util.List;

import fr.pyn.test.bdmulti.domain.db1.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    List<User> findByName(String name);

    User findById(long id);
}