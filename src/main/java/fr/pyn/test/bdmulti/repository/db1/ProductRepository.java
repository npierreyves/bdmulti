package fr.pyn.test.bdmulti.repository.db1;

import fr.pyn.test.bdmulti.domain.db1.Product;
import fr.pyn.test.bdmulti.domain.db1.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    List<Product> findByLabel(String label);

    Product findById(long id);
}