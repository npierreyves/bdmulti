package fr.pyn.test.bdmulti.repository.db2;

import fr.pyn.test.bdmulti.domain.db2.Reference;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ReferenceRepository extends PagingAndSortingRepository<Reference, Long> {

    List<Reference> findByLabel(String label);

    Reference findById(long id);
}