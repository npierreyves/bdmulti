[![pipeline status](https://gitlab.com/npierreyves/bdmulti/badges/main/pipeline.svg)](https://gitlab.com/npierreyves/bdmulti/-/commits/main)
[![coverage report](https://gitlab.com/npierreyves/bdmulti/badges/main/coverage.svg)](https://gitlab.com/npierreyves/bdmulti/-/commits/main)



# bdmulti



##  Database connection

### Definition

```
+------------------------------------------------+  +-------------------------------------------------+
|                      db1                       |  |                      db2                        | 
| +--------------------+ +---------------------+ |  | +---------------------+ +---------------------+ |
| |    schema1_db1     | |     schema2_db1     | |  | |    schema1_db2      | |     schema2_db2     | | 
| | +----------------+ | | +-----------------+ | |  | | +-----------------+ | | +-----------------+ | |
| | |      user      | | | |     product     | | |  | | |    reference    | | | |      projet     | | |
| | +----------------+ | | +-----------------+ | |  | | +-----------------+ | | +-----------------+ | |
| | | #id (int)      | | | | #id (int)       | | |  | | | #id (int)       | | | | #id (int)       | | |
| | | name (varchar) | | | | label (varchar) | | |  | | | label (varchar) | | | | title (varchar) | | |
| | +----------------+ | | +-----------------+ | |  | | +-----------------+ | | +-----------------+ | |
| +--------------------+ +---------------------+ |  | +---------------------+ +---------------------+ |
+------------------------------------------------+  +-------------------------------------------------+
```

### Datasources

#### DB1

#### properties

```yaml
bdmulti:
  datasource:
    db1:
      url: jdbc:postgresql://localhost:5432/db1
      username: user1
      password: password
```

##### configuration

```java
@Configuration
@EnableJpaRepositories(
        basePackages = "fr.pyn.test.bdmulti.repository.db1",
        entityManagerFactoryRef = "db1EntityManagerFactory",
        transactionManagerRef = "db1TransactionManager"
)
@EnableTransactionManagement
public class Db1DataSourceConfiguration {

    @Primary
    @Bean
    @ConfigurationProperties("bdmulti.datasource.db1")
    public DataSourceProperties db1DataSourceProperties(){
        return new DataSourceProperties();
    }

    @Primary
    @Bean
    @ConfigurationProperties("bdmulti.datasource.db1.configuration")
    public DataSource db1DataSource(){
        return db1DataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Primary
    @Bean(name = "db1EntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean db1EntityManagerFactory(EntityManagerFactoryBuilder entityManagerFactoryBuilder) {
        return entityManagerFactoryBuilder.dataSource(db1DataSource()).packages("fr.pyn.test.bdmulti.domain.db1").build();
    }

    @Primary
    @Bean(name = "db1TransactionManager")
    public PlatformTransactionManager db1TransactionManager(final @Qualifier("db1EntityManagerFactory") LocalContainerEntityManagerFactoryBean db1EntityManagerFactory) {
        return new JpaTransactionManager(db1EntityManagerFactory.getObject());
    }

}
```

#### DB2

#### properties

```yaml
bdmulti:
  datasource:
    db2:
      url: jdbc:postgresql://localhost:5432/db2
      username: user1
      password: password
```

#### configuration

```java
@Configuration
@EnableJpaRepositories(
        basePackages = "fr.pyn.test.bdmulti.repository.db2",
        entityManagerFactoryRef = "db2EntityManagerFactory",
        transactionManagerRef = "db2TransactionManager"
)
@EnableTransactionManagement
public class Db2DataSourceConfiguration {
    
    @Bean
    @ConfigurationProperties("bdmulti.datasource.db2")
    public DataSourceProperties db2DataSourceProperties(){
        return new DataSourceProperties();
    }
    
    @Bean
    @ConfigurationProperties("bdmulti.datasource.db2.configuration")
    public DataSource db2DataSource(){
        return db2DataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }
    
    @Bean(name = "db2EntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean db2EntityManagerFactory(EntityManagerFactoryBuilder entityManagerFactoryBuilder) {
        return entityManagerFactoryBuilder.dataSource(db2DataSource()).packages("fr.pyn.test.bdmulti.domain.db2").build();
    }
    
    @Bean(name = "db2TransactionManager")
    public PlatformTransactionManager db2TransactionManager(final @Qualifier("db2EntityManagerFactory") LocalContainerEntityManagerFactoryBean db2EntityManagerFactory) {
        return new JpaTransactionManager(db2EntityManagerFactory.getObject());
    }
}
```
